laptop-status — reading, summarizing, and printing laptop status information
-------------------------------------------------------------------------------

laptop-status is a program to print a one-line summary of important information
concerning the status of the laptop it runs on. It is intended to be used with
such programs as dwm or dvtm.

## BUILD

### Dependencies

Build dependencies:

Binaries:
`make`,
`ocamlfind`,
`ocamlopt`.

Libraries (all available in opam):
`bos` and dependencies (`astring`, `rresult`, `fpath`).


Run dependencies:

`sudo`,
`amixer`,
`rfkill`,
`wpa_cli`,
`acpi`,
`date`.



### Config (optional)

```
cp src/conf.def.ml src/conf.ml
$EDITOR src/conf.ml
```

### Build

`make`


## PACKAGING

In archlinux, `makepkg` creates a pacman package.
