open Bos
open Astring

let ( let* ) = Result.bind
let err line msg = Error (line, msg)
let of_opt line msg = function
  | Some v -> Ok v
  | None -> Error (line, msg)

let stdfmt = Format.std_formatter

let section name f =
  match f name with
  | Ok () -> ()
  | Error (line, msg) ->
      Format.eprintf "Error (%s:%d): %s\n%!" name line msg;
      Format.pp_print_string stdfmt "ERR"
  | exception exc ->
      Format.eprintf "Error (%s): %s\n%!" name (Printexc.to_string exc);
      Format.pp_print_string stdfmt "EXC"

let run line cmd =
  Result.map_error
    (fun (`Msg msg) -> (line, msg))
    OS.Cmd.(in_null |> run_io cmd |> to_lines)
let run_string line cmd =
  Result.map_error
    (fun (`Msg msg) -> (line, msg))
    OS.Cmd.(in_null |> run_io cmd |> to_string)

let () =
  Format.pp_print_string stdfmt "  ";

  section "vol" (fun section ->
  Format.pp_print_string stdfmt section;
  Format.pp_print_string stdfmt ": ";
  begin
    let* lines = run __LINE__ Cmd.(v "pulsemixer" % "--list") in
    let lines = List.filter (String.is_prefix ~affix:"Sink:") lines in
    let lines = List.filter (String.is_suffix ~affix:", Default") lines in
    match lines with
    | [ line ] -> (
        let* defs =
          of_opt __LINE__ "no match for line" @@
          Pat.query (Pat.v "Sink:$(BLANK)ID: $(SINK), Name: $(NAME), Mute: $(MUTE), Channels: $(CHANNELS), Volumes: [$(VOLUMES)], Default") line
        in
        let mute = Option.get @@ String.Map.find "MUTE" defs in
        match mute with
        | "1" ->
            Format.pp_print_string stdfmt "mute";
            Ok ()
        | "0" -> (
            let volumes = Option.get @@ String.Map.find "VOLUMES" defs in
            match volumes with
            | "" ->
               (* sink with 0 channels? *)
               Format.pp_print_string stdfmt "--%";
               Ok ()
            | volumes ->
               let volumes = String.cuts ~sep:", " volumes in
               match volumes with
               | [] ->
                   Format.pp_print_string stdfmt "??%";
                   Ok ()
               | volume :: volumes ->
                  if List.for_all (String.equal volume) volumes then begin
                    (* all channels at the same volume *)
                    assert (volume.[0] = '\'');
                    assert (volume.[String.length volume - 1] = '\'');
                    let volume = String.trim ~drop:(function '\'' -> true | _ -> false) volume in
                    Format.pp_print_string stdfmt volume;
                    Ok ()
                  end else begin
                    (* diff channels have diff volumes *)
                    (* not supported yet *)
                    Format.pp_print_string stdfmt "¿?";
                    Ok ()
                  end
        )
        | not_01 -> err __LINE__ ("Unexpected mute value: " ^ not_01)
    )
    | [] -> err __LINE__ "Audio sink not found"
    | _ :: _ :: _ -> err __LINE__ "Too many audio sinks found"
  end);

  Format.pp_print_string stdfmt " | ";
  section "net" (fun section ->
  Format.pp_print_string stdfmt section;
  Format.pp_print_string stdfmt ": ";
  begin
    let* lines = run __LINE__ Cmd.(v "rfkill" % "list" % "wifi") in
    let* soft = of_opt __LINE__ "no soft blocked match" @@ List.find_opt (String.is_prefix ~affix:"\tSoft blocked:") lines in
    let* defs = of_opt __LINE__ "no soft blocked match" Pat.(query (v "\tSoft blocked: $(YESNO)") soft) in
    match Option.get @@ String.Map.find "YESNO" defs with
    | "yes" -> Format.pp_print_string stdfmt "off"; Ok ()
    | "no" -> begin
        match run __LINE__ Cmd.(v "iwctl" % "station" % Conf.iwstation % "show") with
        | Error _ ->
          Format.pp_print_string stdfmt "(on)"; Ok ()
        | Ok lines ->
          match List.filter (String.is_infix ~affix:"Connected network") lines with
          | [ ssid ] -> (
            match String.fields ~empty:false ssid with
            | "Connected" :: "network" :: ssid ->
                (* This can lose consecutive spaces *)
                let ssid = String.concat ~sep:" " ssid in
                Format.pp_print_string stdfmt ssid; Ok ()
            | _ ->
              Format.pp_print_string stdfmt "(??)"; Ok ()
              )
          | _ ->
            match List.filter (String.is_infix ~affix:"State") lines with
            | [ state ] -> (
            match String.fields ~empty:false state with
            | "State" :: state ->
                (* This can lose consecutive spaces *)
                let state = String.concat ~sep:" " state in
                Format.pp_print_string stdfmt state; Ok ()
            | _ ->
              Format.pp_print_string stdfmt "(??)"; Ok ()
            )
            | _ ->
              Format.pp_print_string stdfmt "(??)"; Ok ()
      end
    | not_yesno -> err __LINE__ ("Unexpected yes/no value: " ^ not_yesno)
  end) ;

  Format.pp_print_string stdfmt " | ";
  section "power" (fun section ->
  Format.pp_print_string stdfmt section;
  Format.pp_print_string stdfmt ": ";
  begin
    let* () =
      let* adapter = run_string __LINE__ Cmd.(v "acpi" % "-a") in
      let* defs =
         of_opt __LINE__ "no adapter match" @@
         Pat.(query (v "Adapter 0: $(ONOFF)-line") adapter)
      in
      match Option.get @@ String.Map.find "ONOFF" defs with
      | "on" -> Format.pp_print_string stdfmt "△:"; Ok ()
      | "off" -> Format.pp_print_string stdfmt "▼:"; Ok ()
      | not_onoff -> err __LINE__ ("Unexpected on/off value: " ^ not_onoff)
    in
    let* () =
      let* battery = run_string __LINE__ Cmd.(v "acpi" % "-b") in
      let* defs =
        of_opt __LINE__ "no battery match" @@
        Pat.(query (v "Battery 0: $(CHARGE), $(PCT)%$(TAIL)") battery)
      in
      let pct = Option.get @@ String.Map.find "PCT" defs in
      Format.pp_print_string stdfmt pct;
      Format.pp_print_string stdfmt "%";
      Ok ()
    in
    Ok ()
  end);

  Format.pp_print_string stdfmt " | ";
  section "date" (fun _ ->
  begin
    let* date = run_string __LINE__ Cmd.(v "date" % Conf.date_format) in
    Format.pp_print_string stdfmt date;
    Ok ()
  end);

  ()
